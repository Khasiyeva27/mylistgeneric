import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class MyList<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private T[] dynamicArr;
    private int size;

    public MyList() {
        dynamicArr = (T[]) new Object[DEFAULT_CAPACITY];
    }

    public void add(T element) {
        if (size == dynamicArr.length) {
            dynamicArr = Arrays.copyOf(dynamicArr, dynamicArr.length * 2);
        }
        dynamicArr[size] = element;
        size++;
    }

    public Optional<T> get(int index) {
        if (index < 0 || index >= size) {
            return Optional.empty();
        }
        return Optional.ofNullable(dynamicArr[index]);
    }

    public Optional<T> remove(int index) {
        if (index < 0 || index >= size) {
            return Optional.empty();
        }
        T removed = dynamicArr[index];

        for (int i = index; i < size - 1; i++) {
            dynamicArr[i] = dynamicArr[i + 1];
        }

        dynamicArr[size - 1] = null;
        size--;

        return Optional.ofNullable(removed);
    }

    public int size() {
        return size;
    }

    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (dynamicArr[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (dynamicArr[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    public MyList<T> sort() {
        Arrays.sort(dynamicArr, 0, size);
        return this;
    }

    public boolean removeElement(T element) {
        int index = indexOf(element);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }

    public boolean removeAll(T element) {
        boolean removedAll = false;
        for (int i = 0; i < size; i++) {
            if (dynamicArr[i].equals(element)) {
                remove(i);
                removedAll = true;
                i--;
            }
        }
        return removedAll;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(dynamicArr[i]).append(" ");
        }
        return sb.toString();
    }

}
