import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        MyList<Object> dynamicArray = new MyList<>();
        dynamicArray.add(10);
        dynamicArray.add(20);
        dynamicArray.add(20);
        dynamicArray.add(20);
        dynamicArray.add(30);
        dynamicArray.add(40);
        dynamicArray.add(50);

        System.out.println(dynamicArray.get(2));

        System.out.println(dynamicArray.size());
        System.out.println(dynamicArray.contains(90));
        System.out.println(dynamicArray.removeElement(70));

        System.out.println(dynamicArray.removeAll(20));

        dynamicArray.remove(0);
        System.out.println(dynamicArray.indexOf(10));

        System.out.println(dynamicArray.sort());

        System.out.println(dynamicArray);
    }
}
